﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" Inherits="Assingment2.js" %>
<asp:Content ContentPlaceHolderID="className" runat="server">
    <h1>SQL</h1>
    <p>SQL is one is one of my favourite languages. Which give us a lot of useful tools.
     I like working with different table trying to combine them with the help of different joings.</p>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="logo_image"> <!-- Contentplaceholder 2 -->
    <asp:Image id="SQLphoto" runat="server" ImageUrl="~/images/sql.jpg"/>
    <h5>A list of helpful links:</h5>
    <ul>
        <li><a href="https://www.w3schools.com/sql/sql_join.asp">Join</a></li>
        <li><a href="https://www.w3schools.com/sql/sql_alias.asp">Aliases</a></li>
        <li><a href="https://www.w3schools.com/sql/sql_update.asp">Update</a></li>
    </ul>
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="classDescription"> <!-- Contentplaceholder 3 -->
    <p>SQL is a standard language for storing, manipulating and retrieving data in databases.</p>
    <p>SQL is an abbreviation for structured query language, and pronounced either see-kwell or as
     separate letters. SQL is a standardized query language for requesting information from a database.
      The original version called SEQUEL (structured English query language) was designed by an IBM research
       center in 1974 and 1975.</p>


</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="codeExample"> <!-- Contentplaceholder 4 -->
    <pre>
        /* Assingment 11.10.2018 Artem Nahornyi. n01261269 */
        
        -- Question 1
        SELECT vendor_name AS "Name"
        FROM vendors
        
        --Question 2
        SELECT vendor_name || ' company phone is - ' || vendor_phone AS "Company phone"
        FROM vendors
        
        --Question 3
        SELECT (848 + 1126) / (45 + 2)
        FROM dual
        
        --Question 4
        --Write a query to find all the things that are not like a pattern, excluding at least two different things (without using AND)
        SELECT *
        FROM customers
        WHERE customer_address NOT LIKE '%W%Ave'
        ORDER BY customer_address
        
        --Question 5
        --Write a query that sorts things by the third last letter
        SELECT vendor_contact_last_name
        FROM vendors
        --ORDER BY SUBSTR(REVERSE(vendor_contact_last_name), 3, 1)
        ORDER BY SUBSTR(vendor_contact_last_name, -3, 1)
        
        --Question 6
        --Using dual, and without hardcoding the current date, find out how many days are between now and Hallowe'en,
        --rounded off to the full day
        SELECT TRUNC(MONTHS_BETWEEN('18-OCT-31', CURRENT_DATE)*31) AS days
        FROM dual
        
        --Question 7
        /* Write a query that shows all the invoices if they don't have a listing in invoice_line_items */
        SELECT *
        FROM invoices
        WHERE invoice_id NOT IN(SELECT DISTINCT invoice_id FROM invoice_line_items)
        
        --Question 8
        /* Write a query that joins invoices, vendors, and vendor_contacts that won't exclude any rows from any of the tables */
        SELECT *
        FROM invoices
        FULL JOIN vendors 
        USING(vendor_id)
        FULL JOIN vendor_contacts 
        USING(vendor_id)
        
        --Question 9
        --Write a query that counts how many invoices there are
        SELECT COUNT(invoice_id)
        FROM invoices
        
        --Question 10
        --Write a query that gets a list of all the vendors with no invoices
        SELECT *
        FROM vendors
        LEFT JOIN invoices
        USING(vendor_id)
        WHERE invoice_id IS null
        
        --Question 11
        --Write a query that lists all the vendor names, and if they have invoices, list the line item description.
        SELECT DISTINCT vendor_name, line_item_description
        FROM vendors v
        JOIN invoices i
        ON v.vendor_id = i.vendor_id
        JOIN invoice_line_items il
        ON i.invoice_id = il.invoice_id
        WHERE i.vendor_id IS NOT null
        
        --Question 12
        --Write a query that gets the average balance owing from all invoices
        SELECT AVG(invoice_total) AS "Averege Balance"
        FROM invoices
        
        --Question 13
        --Write a query that gets the sum of all the invoices in the month of July (any year)
        SELECT SUM(invoice_total) AS "The sum of all invoices in July 2014"
        FROM invoices
        WHERE invoice_date BETWEEN '14-06-01' AND '14-06-30'
        
        --Question 14
        /* Write a query that gets all the vendor ids, along with the most and
        least they've paid on an invoice, assuming those two numbers are different */
        SELECT invoice_id, MAX(payment_total) AS "Max Value", MIN(payment_total) AS "Min Value"
        FROM invoices
        GROUP BY invoice_id
        HAVING MAX(payment_total) <> MIN(payment_total)
        
        --Question 15
        --Write a query that gets all the invoices where the credit total is more than average
        SELECT invoice_id, credit_total
        FROM invoices
        WHERE credit_total > (
            SELECT AVG(credit_total) FROM invoices
        )
        
        --Question 16
        /* Create a table of books. Include books with more than one author. 
        Include all the information you think a library might use. */
        --DROP TABLE books;
        CREATE TABLE books
        (
            book_id         INT               PRIMARY KEY,
            book_name       VARCHAR2(50)      UNIQUE,
            published_year  NUMBER(4)         NOT NULL,
            genre           VARCHAR2(20)      NULL
        ) 
        --Question 17
        /* Create a table of authors.
        Include authors who have written more than one book in the previous table.
        Update the previous table if necessary. */
        CREATE TABLE authors (
            author_id          INT               PRIMARY KEY,
            author_name        VARCHAR(50)       NOT NULL
        )
        --Question 18
        CREATE TABLE books_authors (
            book_id         INT         NOT NULL,
            author_id       INT         NOT NULL
        )
        ALTER TABLE books_authors ADD FOREIGN KEY (book_id) REFERENCES books(book_id);
        ALTER TABLE books_authors ADD FOREIGN KEY (author_id) REFERENCES authors(author_id);  

    </pre>

</asp:Content>