﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" Inherits="Assingment2.js" %>

<asp:Content ContentPlaceHolderID="className" runat="server">
    <h1>HTML/CSS</h1>
    <p>As for me Digital Design is the easiest class. while we are doing something 
    we can see the visualization of the elements and it is really cool. </p>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="logo_image"> <!-- Contentplaceholder 2 -->
    <asp:Image id="HTMLphoto" runat="server" ImageUrl="~/images/html.png"/>
    <h5>A list of helpful links:</h5>
    <ul>
        <li><a href="https://www.w3schools.com/tags/ref_byfunc.asp">HTML Element Reference</a></li>
        <li><a href="https://www.w3schools.com/tags/ref_attributes.asp">HTML Attribute References</a></li>
        <li><a href="https://www.w3schools.com/tags/ref_html_dtd.asp">DOCTYPES</a></li>
    </ul>
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="classDescription"> <!-- Contentplaceholder 3 -->
    <p>Hypertext markup language (HTML) is the major markup language used to display Web pages on the Internet.
     In other words, Web pages are composed of HTML, which is used to display text, images or other resources through 
     a Web browser.</p>
    
    <p>All HTML is plain text, meaning it is not compiled and may be read by humans. The file extension for an HTML
     file is .htm or .html.</p>
     
     <p>Stands for "Cascading Style Sheet." Cascading style sheets are used to format the layout of Web pages.
      They can be used to define text styles, table sizes, and other aspects of Web pages that previously could only
       be defined in a page's HTML.</p>


</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="codeExample"> <!-- Contentplaceholder 4 -->
    <p>This small peace of code will show us the navigation bar and a small part of main content
     information. Also, there is some basic stiles on the right.</p>
    <img src="/images/html_css1.png" alt="html photo" id="htmlCss"/>
    <br/><br/>
    
    <h3>Code from the Net</h3>
    <p>This code wiil create site logic with the helt of: header, article, footer... 
    CSS will just give basic styles to this elements.</p>
    <a href="https://stackoverflow.com/questions/39863458/html-wont-link-to-css-file-in-visual-studio-code">Source</a>

    <img src="/images/html_css.png" alt="html photo" id="htmlCss"/>

</asp:Content>