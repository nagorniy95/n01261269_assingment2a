﻿<%@ Page Title="JavaScript" Language="C#" MasterPageFile="~/Site.master" Inherits="Assingment2.js" %>

<asp:Content runat="server" ContentPlaceHolderID="className"> <!-- Contentplaceholder 1 -->
    <h1>JavaScript</h1>
    <p>In JavaScript we learned a lot about basic JavaScript. The most interesting part for me it
     is "if statements", because they can help us to create page logic.</p> 
     <p>Conditional statements are used to perform different actions based on different conditions.</p>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="logo_image"> <!-- Contentplaceholder 2 -->
    <asp:Image id="JSphoto" runat="server" ImageUrl="~/images/JavaScript.png"/>
    <h5>A list of helpful links:</h5>
    <ul>
        <li><a href="https://www.w3schools.com/js/js_functions.asp">Functions</a></li>
        <li><a href="https://www.w3schools.com/js/js_objects.asp">Object</a></li>
        <li><a href="https://www.w3schools.com/js/js_if_else.asp">JavaScript if else and else if</a></li>
    </ul>
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="classDescription"> <!-- Contentplaceholder 3 -->
    <p>JavaScript is one of the world’s most popular programming languages. 
    The reason for this is quite simply because of its role as the scripting language of the world wide web.
     Today, every personal computer in the world has a JavaScript interpreter installed on it.</p>

    <p>JavaScript very often gets misinterpreted and confused as Java, due to the similarity in name.
     However, JavaScript is not interpreted Java. Java is interpreted Java and JavaScript is a different language.</p>

    <p>Since it is so often asked and misinterpreted, we asked our developers at Acellere to hear what they
     have to say about whether Javascript is a programming language or not.</p>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="codeExample"> <!-- Contentplaceholder 4 -->
    <pre>//#### LAB 5 - FUNCTIONS & OBJECTS ####
        //PART 1:  AN AVERAGE FUNCTION
        // alert("Connected");//COMMENT OUT AS SOON AS YOU KNOW YOU ARE CONNECTED!!!!
        
        //Grade variables
        var databaseGrade = 70;
        var webGrade = 90;
        var webAppGrade = 70;
        var projectManagementGrade = 50;
        var digitalDesignGrade = 60;
        
        //Custom function
        function averageFunction(numberOne, numberTwo, numberThree, numberFour, numberFive)
        {
            var average;
        
            //Get average of parameters
            average = (numberOne + numberTwo + numberThree + numberFour + numberFive) / 5;
        
            //Conditional for pop up box
            if(average >= 70)
            {
                alert("Success");
            }
            else if(average < 70)
            {
                alert("Review Required");
            }
        
            //Return average to 1 decimal place
            // return (Math.round(average * 10) / 10);
            return average.toFixed(1);
        }
        
        //Alert box popup
        averageFunction((databaseGrade, webGrade, webAppGrade, projectManagementGrade, digitalDesignGrade));
        
        //Output average to console 
        console.log(averageFunction(databaseGrade, webGrade, webAppGrade, projectManagementGrade, digitalDesignGrade));
    </pre>

</asp:Content>

